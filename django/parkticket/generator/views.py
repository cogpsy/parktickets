import os
import base64
from datetime import date
from django.shortcuts import render, redirect
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.template.loader import render_to_string
from django.utils.text import slugify
from django.conf import settings
from weasyprint import HTML
from weasyprint.fonts import FontConfiguration

from generator.models import SequenceNo
from generator.forms import GenerateForm
from generator.forms import GateForm

import logging

def index(request):
    tickets_generated = SequenceNo.objects.all().count()
        
    context = {
        'tickets_generated': tickets_generated,
    }

    return render(request, 'index.html', context=context)

def gate(request):
    if request.method == 'GET':
        if 'friend' in request.session and request.session['friend'] == 'mellon':
            return redirect("generate")
        form = GateForm()
    else:
        form = GateForm(request.POST)
        if form.is_valid():
            if form.cleaned_data['secret_word'] == 'secret word':
                request.session['friend'] = "mellon"
                return redirect("generate")
        return redirect("wrong_secret")
    return render(request, "gate.html", {'form': form})

def generate(request):
    if 'friend' not in request.session or request.session['friend'] != 'mellon':
        return redirect("gate")

    if request.method == 'GET':
        form = GenerateForm()
    else:
        form = GenerateForm(request.POST)
        if form.is_valid():
            registration_plate = form.cleaned_data['registration_plate']
            contact_name = form.cleaned_data['contact_name']
            office_phone_number = form.cleaned_data['office_phone_number']
            mobile_phone_number = form.cleaned_data['mobile_phone_number']
            department_name = form.cleaned_data['department_name']

            request.session['form_data'] = form.cleaned_data
            try:
                #send_mail("Parking permit generated", registration_plate + contact_name + office_phone_number, "parking.permits@example.com", ['admin@example.com'])
                p = SequenceNo()
                p.save()
                request.session['form_data']['permit_id'] = p.SequenceNo
            except BadHeaderError:
                return HttpResponse('Unexpected failure. Contact an admin.')
            return redirect("generated")
    return render(request, "generate.html", {'form': form})

def generated(request):

    # logger = logging.getLogger('weasyprint')
    # logger.addHandler(logging.FileHandler('/tmp/weasyprint.log'))
    response = HttpResponse(content_type="application/pdf")
    response['Content-Disposition'] = "inline; filename=parking-permit-{date}-{name}.pdf".format(
        date=date.today().strftime('%Y-%m-%d'),
        name=slugify(request.session['form_data']['registration_plate']),
    )
    html = render_to_string("parking-permit.html", {
        'registration_plate': request.session['form_data']['registration_plate'],
        'contact_name': request.session['form_data']['contact_name'],
        'office_phone_number': request.session['form_data']['office_phone_number'],
        'mobile_phone_number': request.session['form_data']['mobile_phone_number'],
        'department_name': request.session['form_data']['department_name'],
        'permit_id': request.session['form_data']['permit_id']
        })
    font_config = FontConfiguration()
    doc = HTML(string=html, base_url=request.build_absolute_uri()).render()
    image_data = doc.write_png()
    embedded_image = 'data:image/png;base64,' + base64.b64encode(image_data[0]).decode("utf-8")
    html = render_to_string("print-template.html") # uses GET-style context parameters, resulting in a 414: ), {'embedded_image': embedded_image})
    html = html.replace("REALLY_DIRTY_HACK", embedded_image)
    # return HttpResponse(html)
    doc = HTML(string=html, base_url=request.build_absolute_uri()).render()
    doc.write_pdf(response)
    return response

def wrong_secret(request):
    return render(request, 'wrong_secret.html')
