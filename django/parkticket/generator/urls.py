from django.urls import path
from . import views

urlpatterns = [
    path('generator/', views.index, name='index'),
    path('gate/', views.gate, name='gate'),
    path('generate/', views.generate, name='generate'),
    path('generated/', views.generated, name='generated'),
    path('wrong_secret/', views.wrong_secret, name='wrong_secret'),
]
