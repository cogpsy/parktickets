from django.db import models

from django.urls import reverse  # To generate URLS by reversing URL patterns


class SequenceNo(models.Model):
    SequenceNo = models.AutoField(primary_key=True)

    def __str__(self):
        return self.SequenceNo
