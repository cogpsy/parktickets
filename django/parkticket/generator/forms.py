from django.core.exceptions import ValidationError
from django import forms
    
class GenerateForm(forms.Form):
    DEPARTMENTS = [
    ('selectone', '=== select one ==='),
    ('Psychologie I', 'Psychologie I'),
    ('Psychologie II', 'Psychologie II'),
    ('Psychologie III', 'Psychologie III'),
    ('Psychologie IV', 'Psychologie IV'),
    ('Teilbibliothek', 'Teilbibliothek'),
    ('Begabtenberatung', 'Begabtenberatung')
    ]
    registration_plate = forms.CharField(min_length=6, required=True, help_text="Enter your registration plate number in the format <strong>KA-FE 1234</strong>")
    contact_name = forms.CharField(min_length=4, required=True, help_text="Enter the permit holder name. (Your name and title., e.g. <strong>Dr. Who</strong>)")
    office_phone_number = forms.CharField(min_length=6, required=False, help_text="Enter your office phone number, without prefix, in the format <strong>31-12345</strong>")
    mobile_phone_number = forms.CharField(min_length=8, required=False, help_text="Enter your mobile phone number, with prefix, in the format <strong>0123-12345678</strong>")
    department_name = forms.CharField(required=True, widget=forms.Select(choices=DEPARTMENTS), help_text="Select your department from the list")
    
    def clean_department_name(self):
        d = self.cleaned_data.get("department_name")
        if d == "selectone":
            #raise ValidationError({"department_name"}, "Select a department.")
            self.add_error("department_name", "Select a department.")
        return d
    
    def clean(self):
        check = [ self.cleaned_data.get("office_phone_number"), self.cleaned_data.get("mobile_phone_number") ]
        if not any(check):
            #raise ValidationError({"office_phone_number"}, "You must provide at least one phone number, enter either an office or a mobile phone number to proceed.")
            self.add_error("office_phone_number", "At least one phone number is required.")
            self.add_error("mobile_phone_number", "At least one phone number is required.")


class GateForm(forms.Form):
    secret_word = forms.CharField(widget=forms.PasswordInput, required=True, help_text="Enter the secret phrase.")
